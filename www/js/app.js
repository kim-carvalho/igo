// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('Igo', ['ionic','firebase']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

  app.config(function($stateProvider, $urlRouterProvider){
   // Tela de cadastro de novo usuário
   $stateProvider.state('registro', {
   url: '/registro',
   templateUrl: 'templates/cadastro.html',
   controller: 'RegistroCtrl'
  });    
  
  //Tela igo: Origem e destino
  $stateProvider.state('igo', {
    url: '/igo',
    templateUrl: '/templates/igo.html',
    controller: 'IgoCtrl'
  });

  //Tela de login
  $stateProvider.state('login', {
    url: '/login',
    templateUrl: '/templates/login.html',
    controller: 'LoginCtrl'
  });

  //Tela de lista de contatos
  $stateProvider.state('lista', {
    url: '/lista',
    templateUrl: '/templates/lista.html',
    controller: 'ListaCtrl'
  });


  //Tela de Cadastro de Contato
  $stateProvider.state('contato',{
    url: '/contato',
    templateUrl: '/templates/contato.html',
    controller: 'ContatoCtrl'
  });

   // Tela de Viagem
   $stateProvider.state('viagem',{
    url: '/viagem',
    templateUrl: '/templates/viagem.html',
    controller: 'ViagemCtrl'
  });

  // INDICAR A TELA INICIAL DO APLICATIVO
  $urlRouterProvider.otherwise('/login');
  
});

//Controller do login
app.controller('LoginCtrl', function($scope, $state, $firebaseAuth, $ionicPopup) {
  
  $firebaseAuth().$onAuthStateChanged(function(firebaseUser) {
      if(firebaseUser) {
        signOut();      
      }
    })
  
    $scope.entrar = function(user) {
  
      $firebaseAuth()
        .$signInWithEmailAndPassword(user.email, user.password)
          .then(function(firebaseUser){
              // efetuou o login com sucesso.
              $scope.name = firebaseUser.name;
              $state.go('igo');
          })
          .catch(function(error) {
              // ocorreu um erro no login.
  
              $ionicPopup.alert({
                title: 'Login fail',
                template: error.message,
              });
          })
    }
  });
  
  //Controller do novo cadastro
  app.controller('RegistroCtrl', function($scope, $state, $firebaseAuth, $ionicPopup) {
    $scope.registrar = function(user) {
    
        $firebaseAuth()
          .$createUserWithEmailAndPassword(user.email, user.password)
            .then(function(firebaseUser){
                // efetuou o registro com sucesso.              
                $state.go('login');
            })
            .catch(function(error) {
                // ocorreu um erro no registro.
    
                $ionicPopup.alert({
                  title: 'Register fail',
                  template: error.message
                });

            })
      }
    });

    //Controller de contatos 
    app.controller('ListaCtrl', function($scope, $firebaseArray, $firebaseAuth, $state){
    var ref = firebase.database().ref().child('contatos');
   
    //REFERÊNCIA DAS VIAGENS
    var refIdas = firebase.database().ref().child('idas');

    //BUSCANDO INFORMAÇÕES DE CONTATOS
    $scope.idas = $firebaseArray(ref);
    $state.go('lista');

    $scope.contatos = $firebaseArray(ref);

    $scope.listar = function(){
      $state.go('lista');
    }

    $scope.cadastrar = function(){
      $state.go('cadastro');
    }

    $scope.contato = function(){
      $state.go('contato');
    }

    $scope.viagem = function(id){    
      $state.go('viagem');
    }

    $scope.excluir = function(id){
      var obj = $scope.contatos.$getRecord(id);
      $scope.contatos.$remove(obj);
    }

    $scope.signOut = function(){
      firebase.$firebaseAuth.signOut;
      location.reload();
      $state.go('login');
    }

  });//FIM DO CONTROLLER DA LISTA DE CONTATOS

app.controller('ViagemCtrl', function($scope, $firebaseArray, $firebaseAuth, $state){
    $scope.idas = {}; 
    var ref = firebase.database().ref().child('idas');  
    $scope.idas = $firebaseArray(ref);  

    $scope.excluir = function(id){      
        var obj = $scope.idas.$getRecord(id);
        $scope.idas.$remove(obj);      
    }  
    $state.go('viagem');    
});

//Controller DA VIAGEM
app.controller('IgoCtrl', function($scope, $firebaseArray, $firebaseAuth, $state){
  $scope.enviar = function(idas){
    $scope.idas = {}; 
    var ref = firebase.database().ref().child('idas');  
    $firebaseArray(ref).$add(idas).$getRecord;   
    $state.go('lista');    
  }
});

//Controller Novo Contato
app.controller('ContatoCtrl', function($scope, $firebaseArray,$firebaseAuth, $state){
  $scope.contato = {}; 
  $scope.salvar = function(contato){
    var ref = firebase.database().ref().child('contatos');

    contato.img = 'ionic.png';
    $firebaseArray(ref).$add(contato);

    $state.go('lista');
  }
  
});

app.factory('ContatoService', function() {

  var lista = [];
  
  return {

    read: function(){
      return lista;
    },

    create: function(objeto) {
      lista.push(objeto);
    }

  }
});












